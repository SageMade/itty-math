# Itty Math

> Like GLM? Don't like acceptable performance? Then Itty Math is for you!

Itty Math is a lightweight single header math toolkit for rapid prototyping games when using low level rendering APIs. Based off of [GLM](https://glm.g-truc.net/0.9.9/index.html), Itty Math features vector implementations for 2, 3, and 4 dimensions, as well as matrix implementations for 3x3 and 4x4 matrices.

## Usage

Itty Math requires C++ 17 to function, other than that it should be as simple as adding a `#include "IttyMath.hpp"` at the top of your file. 

Itty Math's projection calculations assume that clip space is from -1 to +1 along the z axis when doing matrix calculations. If this is not the case, `#DEFINE MATH_CLIP_MODE` to either `CLIP_NEGATIVE_TO_ONE` or `CLIP_ZERO_TO_ONE` **before** including `IttyMath.hpp`

## Multiple Provided Base Types

Itty Math comes with built-in typedefs for some of the more common vector types for games, these are as follows:
```cpp
typedef vec<2, float> vec2;
typedef vec<3, float> vec3;
typedef vec<4, float> vec4;
typedef mat3_t<float> mat3;
typedef mat4_t<float> mat4;

typedef vec<2, int> ivec2;
typedef vec<3, int> ivec3;
typedef vec<4, int> ivec4;
typedef mat3_t<int> imat3;
typedef mat4_t<int> imat4;

typedef vec<2, uint32_t> uivec2;
typedef vec<3, uint32_t> uivec3;
typedef vec<4, uint32_t> uivec4;
typedef mat3_t<uint32_t> uimat3;
typedef mat4_t<uint32_t> uimat4;
```

## Templated Types

Itty Math uses templates internally, so you can easily add your own vector types. Need a vec3 of unsigned characters? Simply add your own typedef!
```cpp
typedef vec<3, uint8_t> vec3_u8;
typedef vec<2, double> vec2d;
typedef vec<4, uint64_t> vec4_u64;
```
Itty Math will try it's best to implement all the math functions for your type, but note that it may not be accurate, and require template specializations. The Itty Math functions are mainly focused on the `float` type, and may result in incorrect or poor-precision behaviour for other types.

## Swizzling

Swizzling also supported for vector types (using the XYZW notation). Note that assigning or modifying an l-value swizzle with duplicated elements is disabled (and will result in a compile time error).

```cpp
Example:

vec4 v1 = vec4(1, 2, 3, 4);
v1.XY *= vec2(3, 4); // OK
vec3 v2 = v1.XXY; // OK, not an assignment
v2.YX *= 3; // OK, multiplying swizzle by scalar
v1.ZW = v2.YX; // OK, swizzle-swizzle assignment
v2.XX = 0; // Error: deleted function
v1.XX += vec2(1, 1); // Error: deleted function
```

Note that since swizzle assignments can come from the same source (ex: `v2.XY = v2.ZW`), swizzle assignments will result in a copy operation, while direct assignments from vectors will not.

## Macros

Itty-Math has a few different macros you can use to control how it operates:

| Macro             | Options        | Usage                    |
|-------------------|----------------|--------------------------|
| `MATH_CLIP_MODE`  | `CLIP_NEGATIVE_TO_ONE`, `CLIP_ZERO_TO_ONE` | Defaults to `CLIP_NEGATIVE_TO_ONE`, but when set to `CLIP_ZERO_TO_ONE`, will change matrix math to return projections in the 0-1 range as opposed to the -1-1 range |
| `ITTY_MATH_NO_ASSERTS` | Defined or undefined | When defined, this will disable assertions and bounds checking when accessing vectors and matrices, potentially improving performance at the cost of increased risked to memory access violations |

## Fast Matrix Access

When accessing a value from a matrix, most mnath libraries (like GLM), use a dual index operator notation, like in the following experpt from the glm source code:

```cpp
mat<2, 2, T, Q> Inverse(
    + m[1][1] / Determinant,
	- m[0][1] / Determinant,
	- m[1][0] / Determinant,
	+ m[0][0] / Determinant);
```

Internally, this will decompile to two function calls (one to the mat `[]` operator, and one to the vector `[]` operator). This may be optimized by compilers in release modes (have not seen yet), but it would be significantly faster  to directly access the underlying values.

Itty-Math supports this by using the matrix call operator to directly access the underlying fields, improving performance. ex:

```cpp
mat4 value = mat4(1.0f);
float x = value[1][2]; // These are functionally the same
float y = value(1, 2); // But this one should be faster
assert(x == y);
```

## Notes

* **Not tested and not optimized**
* Not recommended for use in production code without some serious love
* Matrices are column major (same as GLM)
* Some math formulas are taken/modified from GLM, I haven't tested them all or looked for optimizations yet
* SIMD is not supported (but may be in the future if I decide to not be lazy)

## License

Itty Math is open source under the MIT license, so feel free to use this in any small projects you're working on!
